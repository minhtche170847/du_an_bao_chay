/*************************************************************

  This is a simple demo of sending and receiving some data.
  Be sure to check out other examples!
 *************************************************************/

/* Fill-in information from Blynk Device Info here */
#define BLYNK_TEMPLATE_ID "TMPL6CHpPaJNw"
#define BLYNK_TEMPLATE_NAME "testBlynl1"
#define BLYNK_AUTH_TOKEN "vKQQB_ubuQHmI5UGENL_0ZhSUlxJeVwX"

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial


#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <DHT.h>
// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Ngoc Anh";
char pass[] = "caominh0410";

BlynkTimer timer;

// This function is called every time the Virtual Pin 0 state changes


// This function is called every time the device is connected to the Blynk.Cloud
BLYNK_CONNECTED() {
  // Change Web Link Button message to "Congratulations!"
  Blynk.setProperty(V3, "offImageUrl", "https://static-image.nyc3.cdn.digitaloceanspaces.com/general/fte/congratulations.png");
  Blynk.setProperty(V3, "onImageUrl", "https://static-image.nyc3.cdn.digitaloceanspaces.com/general/fte/congratulations_pressed.png");
  Blynk.setProperty(V3, "url", "https://docs.blynk.io/en/getting-started/what-do-i-need-to-blynk/how-quickstart-device-was-made");
}

// This function sends Arduino's uptime every second to Virtual Pin 2.
// void myTimerEvent() {
//   // You can send any value at any time.
//   // Please don't send more that 10 values per second.
//   Blynk.virtualWrite(V2, millis() / 1000);
// }

unsigned long startTime1 = 0;
unsigned long startTime2 = 0;
boolean above1 = false;
boolean above2 = false;
int mq2_1 = 34;
int mq2_2 = 35;
int buzzer = 27;
int ledPin = 16;
DHT dht1(26, DHT11);
DHT dht2(25, DHT11);
void setup() {
  // Debug console
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(mq2_1, INPUT);
  pinMode(mq2_2, INPUT);
  Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass);
  delay(2000);
  // You can also specify server:
  //Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass, "blynk.cloud", 80);
  //Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass, IPAddress(192,168,1,100), 8080);
  dht1.begin();
  delay(2000);
  dht2.begin();
  delay(2000);
  // Setup a function to be called every second
  // timer.setInterval(1000L, myTimerEvent);
}

void loop() {
  Blynk.run();

  timer.run();
  Serial.begin(115200);
  float temp1 = dht1.readTemperature();
  delay(300);
  float temp2 = dht2.readTemperature();
  delay(300);
  int mq2_1_value = analogRead(mq2_1);
  int mq2_2_value = analogRead(mq2_2);
  Blynk.virtualWrite(V1, temp1);
  Blynk.virtualWrite(V2, temp2);
  Blynk.virtualWrite(V4, mq2_1_value);
  Blynk.virtualWrite(V3, mq2_2_value);
  delay(200);

  // You can inject your own code or combine it with other sketches.
  // Check other examples on how to communicate with Blynk. Remember
  // to avoid delay() function!

  if (mq2_1_value > 3000) {
    if (!above1) {
      above1 = true;
      startTime1 = millis();
    } else {
      if (millis() - startTime1 >= 3000) {
        alert();
        Blynk.logEvent("bo_chy", "Phát hiện khí gas tại tầng 1");
        Blynk.virtualWrite(V0, "Phát hiện khí gas");
        Serial.println("tang 1");
      }
    }
  } else {
    startTime1 = 0;
    above1 = false;
  }

  if (mq2_2_value > 3000) {
    if (!above2) {
      above2 = true;
      startTime2 = millis();
    } else {
      if (millis() - startTime2 >= 3000) {
        alert();
        Blynk.logEvent("bo_chy", "Phát hiện khí gas tại tầng 2");
        Blynk.virtualWrite(V0, "Phát hiện khí gas");
      }
    }
  } else {
    startTime2 = 0;
    above2 = false;
  }
  if(mq2_2_value < 3000 && mq2_1_value < 3000){
    Blynk.virtualWrite(V0, "  ");
  }
}
// BLYNK_WRITE(V0) {
//   // Set incoming value from pin V0 to a variable
//   int value = param.asInt();

//   // Update state
//   digitalWrite(ledPin, value);
//   Blynk.virtualWrite(V3, digitalRead(ledPin));
// }
void alert() {
  digitalWrite(ledPin, HIGH);
  digitalWrite(buzzer, HIGH);
  delay(500);
  digitalWrite(ledPin, LOW);
  digitalWrite(buzzer, LOW);
}